 ansible -i ./hosts -m ping all --one-line
 ansible-console -i ./hosts
 ansible -i ./hosts -m raw -s -a "echo ok" all --ask-sudo-pass
 ansible -i ./hosts -m raw -s -a "apt update" all --ask-sudo-pass
 ansible -i ./hosts -m setup -s  all --ask-sudo-pass

Deploy borg backup on ext.arn-fai.net
```
ansible-playbook -i ./hosts --ask-become-pass --ask-vault-pass --limit ext.arn-fai.net borg.yml
```
